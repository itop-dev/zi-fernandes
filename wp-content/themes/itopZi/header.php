<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage iTop_Zi_Fernandes
 * @since iTop 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="author" content="itopservice.com.br | Desenvolvido por Renato Almeida">         
    <meta name="keywords" content="potography, photographer, images, zi fernandes,"> 
    <meta http-equiv="content-language" content="en-US">
    <meta name="reply-to" content="renato.almeida@al.infnet.edu.br">

	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php global $page, $paged;
            wp_title('|', true, 'right');
            bloginfo('name');
            $site_description = get_bloginfo('description', 'display');
            if ($site_description && ( is_home() || is_front_page() ))
                echo " | $site_description";
            if ($paged >= 2 || $page >= 2)
                echo ' | ' . sprintf(__('Page %s', 'itopZi'), max($paged, $page));
         ?>	
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png" />

	<!-- Fonts -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,700" />

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/css/flexslider.css' type='text/css' media='all' />
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/css/jquery.fancybox-1.3.4.css' type='text/css' media='all' />
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/css/epicslider.css' type='text/css' media='all' />
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/css/udt_shortcodes.css' type='text/css' media='all' />
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/css/skin.css' type='text/css' media='all' />
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/css/udt_media_queries.css' type='text/css' media='all' />

	<!-- Scripts -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery-1.11.0.min.js'></script>
	<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery-migrate-1.2.1.min.js'></script>

	<!-- Scripts Footer-->
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.ui.core.min.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.fancybox-1.3.4.pack.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.epicHover-fadeZoom.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.epicslider.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.flexslider-min-edited.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.mobile-touch-swipe-1.0.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/settings.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/common.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/udt_shortcodes.js'></script>
<script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?sensor=false'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/contact.js'></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> class="home page">
<!-- Header -->
<div id="header-wrapper">

	<div id="header-inner">
		<header>
			<!-- Logo -->
			<div id="logo">
				<a href="<?php echo get_option('home'); ?>/index.php" title="Raw HTML Template Demo White">
					<img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Raw HTML Template Demo White">
				</a>
			</div>

			<!-- Mobile Menu Toggle -->
			<div class="mobile-menu-toggle"><a href=""></a></div>

			<!-- Navigation -->
			<nav id="primary-nav">
				<div class="menu-container">
					<ul id="menu-1" class="menu">
						<li class="menu-item current-menu-item current_page_item"><a href="<?php echo get_option('home'); ?>/index.php">Home</a></li>
						<li class="menu-item"><a href="<?php bloginfo('home') ?>/?page_id=2">About me</a></li>
						<li class="menu-item"><a href="<?php bloginfo('home') ?>/?page_id=2">Gallery</a></li>
						<li class="menu-item"><a href="<?php bloginfo('home') ?>/?page_id=2">Blog</a></li>
						<li class="menu-item"><a href="<?php bloginfo('home') ?>/?page_id=2">Contact</a></li>
					</ul>
				</div>
			</nav>

		</header>
		<div style="clear:both;"></div>
	</div>

	<div style="clear:both;"></div>

	<div class="header-widget-box">
		<div class="header-outer-widget-wrapper">
			<div class="header-widget-wrapper">
				<ul>
					<li class="widget widget_text">
						<h4 class="widget-title">Get In Touch</h4>
						<div class="textwidget">
							<p>This is a widget area. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
						</div>
					</li>
				</ul>
			</div>
			
			<ul class="connect black">
				<li><a href="#" title="500px" class="fivehundredpx">500px</a></li>
				<li><a href="#" title="DeviantArt" class="deviantart">DeviantArt</a></li>
				<li><a href="#" title="Dribbble" class="dribbble">Dribbble</a></li>
				<li><a href="#" title="Facebook" class="facebook">Facebook</a></li>
				<li><a href="#" title="Flickr" class="flickr">Flickr</a></li>
				<li><a href="#" title="LinkedIn" class="linkedin">LinkedIn</a></li>
			</ul>
			<div style="clear:both;"></div>

			<div class="mobile-widget-box-toggle-wrapper">
				<a href="#">Toggle</a>
			</div>
		</div>
	</div>

	<div style="clear:both;"></div>
</div>
<!-- Header End -->

<!-- Content Area -->
<div id="content-wrapper" class="front-page front-page-full-width-media">
