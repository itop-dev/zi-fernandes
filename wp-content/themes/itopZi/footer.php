<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since iTop 1.0
 */
?>
</div>
<!-- Content Area End -->

<!-- Footer -->
<div id="footer-wrapper">

	<div id="footer-top" class="clearfix">
		<footer>
			<div class="column_one_third column-footer-widget">
				<ul class="footer-widget">
					<li class="widget widget_text">
						<div class="textwidget">
							<p>
								<img src="<?php bloginfo('template_url'); ?>/images/logo-footer.png" alt="Raw" /><br />
								Raw HTML Template is a responsive HTML5 template ideal for the photographer or studio. With full & fixed width portfolio grids, full width sliders with HTML5 video background & touch support, fashionable captions and much more, this template will make your work stand out.
							</p>
						</div>
					</li>
				</ul>
			</div>

			<div class="column_one_third column-footer-widget">
				<ul class="footer-widget">
					<li class="widget widget_recent_entries">
						<h4 class="widget-title">Recent Posts</h4>
						<ul>
							<li>
								<a href="<?php bloginfo('home') ?>/?page_id=2" title="A Vimeo Post">A Vimeo Post</a>
								<span class="post-date">October 24, 2013</span>
							</li>
							<li>
								<a href="<?php bloginfo('home') ?>/?page_id=2" title="Photo Post">Photo Post</a>
								<span class="post-date">October 24, 2013</span>
							</li>
							<li>
								<a href="<?php bloginfo('home') ?>/?page_id=2l" title="A YouTube Post">A YouTube Post</a>
								<span class="post-date">October 24, 2013</span>
							</li>
							<li>
								<a href="<?php bloginfo('home') ?>/?page_id=2" title="Paginated Post">Paginated Post</a>
								<span class="post-date">October 23, 2013</span>
							</li>
							<li>
								<a href="<?php bloginfo('home') ?>/?page_id=2" title="A Sticky Post">A Sticky Post</a>
								<span class="post-date">October 20, 2013</span>
							</li>
							<li>
								<a href="<?php bloginfo('home') ?>/?page_id=2" title="A Regular Post">A Regular Post</a>
								<span class="post-date">October 20, 2013</span>
							</li>
						</ul>
					</li>
				</ul>
			</div>

			<div class="column_one_third column-footer-widget last">
				<ul class="footer-widget">
					<li class="widget widget_text">
						<h4 class="widget-title">Let’s Collaborate</h4>
						<div class="textwidget">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna.</p>
							<p>Tel: +1 55 65 15 45 85<br />
							Fax:+1 65 98 56 45 45<br />
							Mail: zifernandes17@gmail.com</p>
						</div>
					</li>
				</ul>
			</div>
		
			<div class="clear"></div>
		</footer>
	</div>

	<div id="footer-bottom">
		<div id="footer-bottom-inner-wrapper">
			<footer>
				<!-- Copyright info -->
				<p class="footer-copyright">&copy; <a href="http://itopservice.com.br" title="iTop - Soluções Inteligentes">iTop</a>. All Rights Reserved.</p>
			</footer>
			<!-- "Back to Top" link -->
			<a class="back-to-top" title="Back to top" href="#">Back to top</a>
		</div>
	</div>

</div>

	<?php wp_footer(); ?>
</body>
</html>