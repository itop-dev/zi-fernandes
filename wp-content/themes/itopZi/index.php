<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage iTop_Zi_Fernandes
 * @since iTop 1.0
 */

get_header(); ?>

	<!-- Epic Slider -->
	<section class="slider-full-width slider-homepage clearfix">
		<div id="epicSlider1" class="epic-slider slider-wrapper">
			<ul id="slides">
				<li data-image="images/portfolio/30.jpg">
					<div class="es-caption" style="" data-caption="impact" data-caption-position="center" data-caption-width="550">
						<p><span class="size-2" style="margin-bottom:0;">WELCOME TO RAW</span><span class="size-6" >MIAMI. NEW YORK. LOS ANGELES</span></p>
					</div>
				</li>
				<li data-image="images/portfolio/beach-woman.jpg">
					<div class="es-caption" style="" data-caption="impact" data-caption-position="center" data-caption-width="770">
						<p><span class="size-2" style="margin-bottom:0;">A KICKASS HTML5 </span><span class="size-5" >PHOTOGRAPHY THEME</span> <a href="portfolio.html" class="submit submitDarkGray" style="margin-bottom:0;" title="">See Our Work</a></p>
					</div>
					<div class="es-video-background-wrapper">
						<video id="fs1" class="es-video-background">
							<source src="<?php bloginfo('template_url'); ?>/images/portfolio/beach-woman.m4v" type="video/mp4" />
							<source src="<?php bloginfo('template_url'); ?>/images/portfolio/beach-woman.webm" type="video/webm" />
							<source src="<?php bloginfo('template_url'); ?>/images/portfolio/beach-woman.ogv" type="video/ogg" />
						</video>
					</div>
				</li>
				<li data-image="images/portfolio/31.jpg">
					<div class="es-caption" style="" data-caption="striped" data-caption-position="bottom-left" data-caption-width="450">
						<p>
							<span class="size-6" >IDEAL FOR PHOTOGRAPHERS,</span><br />
							<span class="size-6" >AGENCIES AND CREATIVE FREELANCERS</span><br />
							<a href="#" class="es-next-slide"><span class="size-6" >Next Slide</span></a>
						</p>
					</div>
				</li>
				<li data-image="images/portfolio/33.jpg">
					<div class="es-caption" style="" data-caption="elegant" data-caption-position="bottom-left" data-caption-width="350">
						<p>
							<span class="size-2" >THE PERFECT SHOWCASE</span><br />
							A must have for the serious photographer or studio. Full width slider with fullscreen toggle, sexy portfolio layouts and much more. <a class="es-next-slide" href="#">Next Slide</a>.
						</p>
					</div>
				</li>
			</ul>
		</div>
	</section>
	<!-- Epic Slider End -->

	<!-- Portfolio Index Grid -->
	<section class="portfolio-full-width-grid">
		<div class="sub-section-title">
			<h2>Latest Projects</h2>
		</div>

		<div id="grid" class="clearfix">
			<div class="thumb" data-project-categories="video">
				<a href="<?php bloginfo('home') ?>/?page_id=2" title="Video and Slider" >
					<img src="<?php bloginfo('template_url'); ?>/images/portfolio/341-384x263.jpg" alt="Video and Slider">
				</a>
			</div>
			<div class="thumb" data-project-categories="music">
				<a href="<?php bloginfo('home') ?>/?page_id=2" title="SoundCloud" >
					<img src="<?php bloginfo('template_url'); ?>/images/portfolio/content-width-old-stereo-384x263.jpg" alt="SoundCloud">
				</a>
			</div>
			<div class="thumb" data-project-categories="photography">
				<a href="<?php bloginfo('home') ?>/?page_id=2" title="Photo Filters" >
					<img src="<?php bloginfo('template_url'); ?>/images/portfolio/401-384x263.jpg" alt="Photo Filters">
				</a>
			</div>
			<div class="thumb" data-project-categories="video">
				<a href="<?php bloginfo('home') ?>/?page_id=2" title="New York" data-caption="Biotopes New York <span>full width video project</span>">
					<img src="<?php bloginfo('template_url'); ?>/images/portfolio/22-384x263.jpg" alt="New York">
				</a>
			</div>
			<div class="thumb" data-project-categories="photography">
				<a href="<?php bloginfo('home') ?>/?page_id=2" title="Black and White" >
					<img src="<?php bloginfo('template_url'); ?>/images/portfolio/content-width-woman-attitude1-384x263.jpg" alt="Black and White">
				</a>
			</div>
			<div class="thumb" data-project-categories="fashion identity video">
				<a href="<?php bloginfo('home') ?>/?page_id=2" title="Background Video" data-caption="Nero Lingerie <span>full width background video</span>">
					<img src="<?php bloginfo('template_url'); ?>/images/portfolio/49-384x263.jpg" alt="Background Video">
				</a>
			</div>
			<div class="thumb" data-project-categories="photography video">
				<a href="<?php bloginfo('home') ?>/?page_id=2" title="Another Video Project" >
					<img src="<?php bloginfo('template_url'); ?>/images/portfolio/content-width-ny-384x263.jpg" alt="Another Video Project">
				</a>
			</div>
			<div class="thumb" data-project-categories="video">
				<a href="<?php bloginfo('home') ?>/?page_id=2" title="Full Width YouTube Project" >
					<img src="<?php bloginfo('template_url'); ?>/images/portfolio/content-width-chamonix-384x263.jpg" alt="Full Width YouTube Project">
				</a>
			</div>
		</div>

		<div class="portfolio-button"><a href="<?php bloginfo('home') ?>/?page_id=2" title="View all Projects">View all Projects &rarr;</a></div>
	</section>
	<!-- Portfolio Index Grid End -->

<?php get_footer(); ?>
